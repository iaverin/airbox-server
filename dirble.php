<?php
require 'inc/util.php';
require 'inc/db.php';
require 'inc/enums.php';
require "inc/dirble.class.php";


header("Access-Control-Allow-Origin: *");

if(!isset($_GET["command"])){
	
	exit(0);
} 


$command=$_GET['command'];
$param=$_GET['param']; 

$dirble=new dirbleClient($dirble_api_key);


if($command=="getAllCategories"){
	echo $dirble->getAllCategoriesJson();
}


if($command=="getAllCategoriesV2"){
	
	echo  json_encode($dirble->getAllCategories());

	exit(0);
}

if($command=="getAllCountries"){
	echo   json_encode($dirble->getAllCountries());
};


if($command=="getAllGenresFromDbJSON"){
	echo $dirble->getAllGenresFromDbJSON();
};


if($command=="getCountriesFromDbJSON"){
	echo $dirble->getCountriesFromDbJSON();
};


if($command=="getStationsByCountry"){
	$countryCode=$_GET['param'];
	echo $dirble->getActiveStationsByCountry($countryCode,false);
};


if($command=="getStationsByCategory"){
	$categoryId=$_GET['param'];
	echo $dirble->getActiveStationsByCategory($categoryId,false);
};


?>