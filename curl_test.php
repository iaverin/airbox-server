<?php 

require 'Curl.class.php';

$curl = new Curl();
$curl->setBasicAuthentication('username', 'password');
$curl->setUserAgent('');
$curl->setReferrer('');
$curl->setHeader('X-Requested-With', 'XMLHttpRequest');
$curl->setCookie('key', 'value');
$curl->get('http://www.example.com/');

if ($curl->error) {
	echo $curl->error_code;
}
else {
	echo $curl->response;
}

var_dump($curl->request_headers);
var_dump($curl->response_headers);


?>