<?php
session_start();
require 'inc/util.php';
require 'inc/db.php';
require 'inc/smarty.php';
require 'inc/enums.php';
require "inc/8tracks.php";
require "inc/radio.php";
require "inc/dirble.class.php";
require 'inc/airboxAPI.class.php';

if (isset($_GET['device_key'])) {
	$device_key = $_GET['device_key'];

} else {

exit(0);
}

$playerId = db_get_player_id($device_key);

$dbCurrent = db_get_current();

$currentList = $dbCurrent["current_list"];
$currentSong = $dbCurrent["current_song"];
$currentCommand = $dbCurrent["command"];
$status = $dbCurrent["status"];

// Fill the array to pass for render
$data = array();

$data['player_id'] = $playerId;
$data['device_key'] = db_get_device_key($playerId);
$data["currentList"] = $currentList;
$data["currentSong"] = $currentSong ;
$data["status"] = $status;
$data["command"] = $currentCommand;
$data["timestamp"] = $dbCurrent["time_diff"];
$data['playerStatus'] =$dbCurrent['additional_data'];

if (strpos($currentList, "cmdfm://") !== false) {

	$cmdfmCurrentTitle=cmdfm_get_CurrentSongTitle($playerId, $currentSong);

	$data['currentSongTitle'] = $cmdfmCurrentTitle['songTitle'];
	$data['currentListTitle'] = $cmdfmCurrentTitle['listTitle'];

	//$data['currentListTitle'] = substr($currentList, strlen("cmdfm://"));

	$data['appTitle'] = "cmd.fm";

	if ($currentSong < count(cmdfm_getCurrentPlaylistUrls($playerId)) ) { $data['nextTrack']=$currentSong+1;}

	//$data['tracksCount']=$mixInfo['tracks_count'];

	//todo:  add tracks count for cmd.fm
} elseif (strpos($currentList, "8tracks://") !== false) {

	$mixInfo = _8tracks_get_CurrentMixInfo($playerId);
	$data['appTitle'] = "8tracks";

	$data['currentListTitle'] = $mixInfo["name"];
	$data['currentSongTitle'] = "Track No:" . ($currentSong + 1) . "/" . $mixInfo['tracks_count'];
	$data['tracksCount'] = $mixInfo['tracks_count'];

	if ($currentSong < $mixInfo['tracks_count'] ) { $data['nextTrack']=$currentSong+1;}
	
} 

elseif (strpos($currentList, "radio://") !== false) {

	$stationId=substr($currentList, strlen("radio://"));
	$radio = radio_getStationTitle($stationId );
	
	$data['appTitle'] = "radio";
	$data['currentListTitle'] = $radio['title'];
	$data['currentSongTitle']="";
	
	$data['tracksCount'] = 1;
	$data['nextTrack']=0;
} 

elseif (strpos($currentList, "dirble://") !== false) {
	$dirble=new dirbleClient($dirble_api_key);

	$stationId=substr($currentList, strlen("dirble://"));
	
	
	
	$dirble->getStationFromDb($playerId);
		
	//print_r($dirble);	

	$data['appTitle'] = "Online Radio";
	$data['currentListTitle'] = $dirble->currentStation['title'];
	$data['currentSongTitle']="";
	
	$data['tracksCount'] = 1;
	$data['nextTrack']=0;
} elseif (strpos($currentList, "airbox://") !== false) {

	$api= new airboxAPI($playerId);
	$api->getCurrentStatus();

	
	$data['appTitle'] = $api->appTitle;

	$data['currentListTitle'] = $api->playlistTitle;

	$data['currentSongTitle'] = $api->tracksInfo[$currentSong]['title'];

	$data['tracksCount'] = $api->totalTracks;

	if ($currentSong < $api->totalTracks ) { $data['nextTrack']=$currentSong+1;}
	
} 





else {
	$data['currentListTitle'] = $currentList;
	$data['appTitle'] = "list";


	//$data['currentSongTitle'] = get_song_title_from_csv($currentList, $currentSong);
}

header("Access-Control-Allow-Origin: *");
echo json_encode(array("device_status" => $data));
?>