<?php
session_start ();
require 'inc/util.php';
require 'inc/db.php';
require 'inc/smarty.php';
require 'inc/enums.php';
require "inc/8tracks.php";

if ($_SESSION ['player_id'] == "") {
	header ( "Location: http://" . $_SERVER ["SERVER_NAME"] . ":" . $_SERVER ["SERVER_PORT"] . "/login.php" );
} else {
	
	$playerId = $_SESSION ['player_id'];
}

// -----------------------------------------------------------------------------------------------
// Set net new playlist/song after command PLAY or INIT status and send full
// playlist to player
// Set commands to DataBase
// ------------------------------------------------------------------------------------------------

if (isset ( $_GET ["current_list"] ) && isset ( $_GET ["current_song"] ) && isset ( $_GET ["command"] )) {
	$currentList = htmlspecialchars_decode ( $_GET ["current_list"] );
	$currentSong = $_GET ["current_song"];
	$currentCommand = $_GET ["command"];
	
	$dbNew = array ();
	$dbNew ["current_list"] = $currentList;
	
	$dbNew ["current_song"] = $currentSong;
	$dbNew ["command"] = $currentCommand;
	if (isset ( $_GET ["status"] )) {
		$dbNew ["status"] = $_GET ["status"];
	}
	
	db_update_current ( $dbNew );
	// todo ���������� ������� �� ����� ����������
	header ( "Location: http://" . $_SERVER ["SERVER_NAME"] . ":" . $_SERVER ["SERVER_PORT"] . "/ui.php?current_list=$currentList" );
	exit ();
}

// -------------------------------
// Output UI, current list/song
// ---------------------------------

$dbCurrent = db_get_current ();

$currentList = $dbCurrent ["current_list"];
$currentSong = $dbCurrent ["current_song"];
$currentCommand = $dbCurrent ["command"];
$status = $dbCurrent ["status"];

// Get List to show
if (isset ( $_GET ["current_list"] ) && strpos($_GET ["current_list"],"cmdfm://")===false)  {
	$currentListShow = htmlspecialchars_decode ( $_GET ["current_list"] );
}

if (strpos($currentList,"cmdfm://")!==false && !isset($_GET['ignore_rd']) and !isset($_GET['current_list'])){
	
	header ( "Location: http://" . $_SERVER ["SERVER_NAME"] . ":" . $_SERVER ["SERVER_PORT"] . "/ui_cmdfm.php?current_list=$currentList" );
	
	exit(0);
}


// echo get_song_title($currentList,$currentSong);

// ob_flush();
// var_dump($playlists);

// ----------------------------------------
// Fill the array to pass for render
$data = array ();

$data ['player_id'] = $playerId;
$data ['device_key'] = $_SESSION ['device_key'];
$data ["currentList"] = $currentList;
$data ["currentSong"] = $currentSong;
$data ["status"] = $status;
$data ["command"] = $currentCommand;
$data ["timestamp"] = $dbCurrent ["time_diff"];



$playlists = read_csv ( "data/playlists.csv" );
$data ['playlists'] = $playlists; // odo escape



if (strpos($currentList,"cmdfm://")!==false)
	{
		$data ['currentSongTitle'] = cmdfm_get_CurrentSongTitle($playerId, $currentSong );

	}
	elseif (strpos ( $currentList, "8tracks://" ) !== false){
			
		$mixInfo=_8tracks_get_CurrentMixInfo($playerId);
		
		$data ['currentSongTitle']="<br>".$mixInfo["name"].". Track No:".($currentSong+1)."/". $mixInfo['tracks_count'];
	}
	
	
	else{
		

			$data ['currentSongTitle'] = get_song_title_from_csv ( $currentList, $currentSong );
	
		   
		
	}
	

	if(isset($currentListShow))	{$data ['listContents'] = get_playlist_contents_to_show($currentListShow);}
	$data ["currentListShow"] = $currentListShow;	
	



 // n seconds
                                              // ----------------------------------------
                                              // Show
render ( $data, "ui.tpl" );




function get_playlist_contents_to_show($playlist) {
	global $playerId, $playlists;

	if (strpos ( $currentList, "cmdfm://" ) !== false) {
		return cmdfm_getCurrentPlaylistUrls ( $playerId );
	} 
	
	elseif (strpos ( $currentList, "8tracks://" ) !== false){
			
			return $currentList;
	}
	else{
	
	
	return read_csv ( "data/" . $playlists [$playlist], true );}
	
	
}




?>



