<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
"http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	
	{if not  $playlists } <meta http-equiv="refresh" content="5"> {/if}
					
	{literal}
	<style type="text/css">
		body {font-family:Arial, monospace, sans-serif; }
		.hint {font-size: 0.7em;}
		
	</style>
	{/literal}
</head>
<body {if $timestamp >5 } bgcolor="grey"{/if}>
<h3>== cmd.fm @ airbox ==</h3>
 
<h3>		<p>Now Playing: <b>{$currentList}</b> {$currentSongTitle}</p> </h3>
		{if $next_track}<h1><a href="ui_cmdfm.php?current_song={$next_track}&command=play">Next track</a></h1>{/if}
		

{if not  $playlists }<h1><a href=ui_cmdfm.php?show_genres=1>Show genres</a></h1>{/if}
{if   $playlists }<h1><a href=ui_cmdfm.php>Hide genres</a></h1>{/if}

<ul>
{foreach from=$playlists key=key item=name}
<li>
	{if $currentListShow != $name}
	<a href="ui_cmdfm.php?current_list={'cmdfm://'|escape:'url'}{$name|escape:'url'}&current_song=0&command=play">{$name}</a> </li>
	{else}
	{$name}
	{/if}
</li>		
{/foreach}
</ul>


{if isset($listContents)}
<h3>== Genres ==</h3>
<ul>
	{foreach from=$listContents key=key item=value}
<li>
	{if isset($currentSong) && $currentSong == $key && $status == 'playing' && $currentList==$currentListShow}
		{$key} - {$value['title']} &nbsp; <a href="ui.php?current_list={$currentList}&current_song={$key}&command=stop">Stop</a>
	{else}
		<a href="ui.php?current_list={$currentListShow}&current_song={$key}&command=play">{$key} - {$value['title']}</a>
	{/if}
</li>
	{/foreach}
</ul>
{/if}

<h3><a href="ui.php?ignore_rd=1">Radios & Lists</a></h3>

-----------------------------------------------

{if $command == 'ok'}		
		<div class="hint">Last status {$status}, {$timestamp}s ago.
		<a href="ui_cmdfm.php?current_list={$currentList}&current_song={$currentSong}&command=stop">Stop</a>
		
</div>
	{else}
		<div class="hint">Song number {$currentSong} is scheduled for command [{$command}], status [{$status}], last sync [{$timestamp}] seconds ago.
		</div>
	{/if} 

<small>Device: {$device_key}</small>
<a class="hint" href="login.php">set device</a><br/>
<br/>

</body>
</html>
