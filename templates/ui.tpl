<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
"http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	
<meta http-equiv="refresh" content="5"> 
					
	{literal}
	<style type="text/css">
		body {font-family:Arial, monospace, sans-serif; }
		.hint {font-size: 0.7em;}
	</style>
	{/literal}
</head>
<body {if $timestamp >5 } bgcolor="grey"{/if}>
<h3>== airbox ==</h3>
<small>Device: {$device_key}</small>

{if $command == 'ok'}
		<p>Now Playing: {$currentList}  {$currentSongTitle}</p>
		<div class="hint">Last status {$status}, {$timestamp}s ago.
		<a href="ui.php?current_list={$currentList}&current_song={$currentSong}&command=stop">Stop</a>
</div>
	{else}
		<div class="hint">Song number {$currentSong} is scheduled for command [{$command}], status [{$status}], last sync [{$timestamp}] seconds ago.
		</div>
	{/if}

<h3><a href="ui_cmdfm.php">CMD.FM</a></h3>
<h3><a href="ui_8tracks_mixes.php">8tracks</a></h3>


<ul>
{foreach from=$playlists key=key item=name}
<li>
	{if $currentListShow != $key}
	<a href="ui.php?current_list={$key}&current_song=0">{$key}</a>&nbsp;&nbsp;&nbsp;<a href="ui.php?current_list={$key}&current_song=0&command=play">Play list</a> </li>
	{else}
	{$key}
	{/if}
</li>		
{/foreach}
</ul>

<h3>== playlist ==</h3>
{if isset($listContents)}

<ul>
	{foreach from=$listContents key=key item=value}
<li>
	{if isset($currentSong) && $currentSong == $key && $status == 'playing' && $currentList==$currentListShow}
		{$key} - {$value['title']} &nbsp; <a href="ui.php?current_list={$currentList}&current_song={$key}&command=stop">Stop</a>
	{else}
		<a href="ui.php?current_list={$currentListShow}&current_song={$key}&command=play">{$key} - {$value['title']}</a>
	{/if}
</li>
	{/foreach}
</ul>
{/if}


<a href="login.php">SET DEVICE</a><br/>
<br/>
<a class="hint" href="airbox-android.apk">Download client</a>
</body>
</html>
