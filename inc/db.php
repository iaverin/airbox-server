<?php

require "_settings.php";
require_once 'util.php';

//$playerId = 1;
//echo $playerId;

#Database schema
#  TABLE player_state
#	player_id: number
#	curent_list: string
#	current_song: number
#	command: string
#	status: string

//phpinfo();


mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
	mysql_select_db($dbname) or die(mysql_error());

function db_get_current () {
	global $dbhost;
	global $dbuser;
	global $dbpass;
	global $dbname;
	global $playerId;

	mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
	mysql_select_db($dbname) or die(mysql_error());
	$query="select *, TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `timestamp`)) AS time_diff".
		" from player_state where player_id=".$playerId;


	$result=mysql_query($query);
	$row=mysql_fetch_assoc($result);
	mysql_free_result($result);





	return $row;
}

function db_update_current ($dbCurrent) {
	global $dbhost;
	global $dbuser;
	global $dbpass;
	global $dbname;
	global $playerId;
	global $abSessionStatsTerm;
	$currentList = $dbCurrent ["current_list"];
	$currentSong = $dbCurrent ["current_song"];
	$currentCommand = $dbCurrent ["command"];
	$additionalData = $dbCurrent["additional_data"];

	$link = mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
	mysql_select_db($dbname) or die(mysql_error());
	$check = mysql_query("select * from player_state where player_id=".$playerId);


	if (mysql_num_rows($check) < 1) {

		$query = query_insert($playerId, $currentList, $currentSong, $currentCommand, $status, $additionalData );


	}
	else {


		if (!isset($dbCurrent ["status"]) || strcmp ($dbCurrent ["status"], "") == 0) {
			$query = query_update_from_ui ($playerId, $currentList, $currentSong, $currentCommand);
			mysql_query(query_insert_sync_stats($playerId, $currentList, $currentSong, $currentCommand,"from_ui"));
			//print_r($query);
		}


		else {

			$status = $dbCurrent ["status"];
			$query = query_update_from_sync ($playerId, $currentList, $currentSong, $currentCommand, $status,$additionalData);
			//mysql_query($query);

			mysql_query(query_insert_sync_stats($playerId, $currentList, $currentSong, $currentCommand,"from_player"));
			//print_r($dbCurrent);
			//print_r( $dbCurrent['time_diff']);



			if( $dbCurrent['time_diff'] > $abSessionStatsTerm ) {

				$query2="insert into sync_sessions set player_id=".$playerId.", session_start=CURRENT_TIMESTAMP, session_last_sync=CURRENT_TIMESTAMP";
				$r=mysql_query($query2);

				} else{

					$query2="select max(sync_session_id) as last_session_id from sync_sessions where player_id=".$playerId." limit 1";
						$r=mysql_query($query2);
						$f=mysql_fetch_array($r);

						$syncSessionId=$f['last_session_id'];

						$query2="update sync_sessions set session_last_sync=CURRENT_TIMESTAMP where sync_session_id=".$syncSessionId;
						mysql_query($query2);

				}
		}
	}

	$queryResult = mysql_query($query);

}

#TODO move out all queries to separate files
function query_insert ($playerId, $currentList, $currentSong, $currentCommand, $status, $additionalData="") {
	return "insert into player_state (player_id, current_list, current_song, command, status, additional_data)".
				" values (".$playerId.", '".$currentList."', ".$currentSong.",'".$currentCommand."','stopped',"."'".$additionalData."'";
}

#TODO refactor
function query_update_from_sync ($playerId, $currentList, $currentSong, $currentCommand, $status,$additionalData="") {

	if($additionalData!="") {
	return "update player_state set current_list='".$currentList."', current_song=".$currentSong.
			", command='".$currentCommand."', status='".$status."', timestamp=CURRENT_TIMESTAMP, additional_data='".$additionalData."' where player_id=".$playerId.";";}
	else
	 {
				return "update player_state set current_list='".$currentList."', current_song=".$currentSong.
			", command='".$currentCommand."', status='".$status."', timestamp=CURRENT_TIMESTAMP where player_id=".$playerId.";";
			}
}

function query_update_from_ui ($playerId, $currentList, $currentSong, $currentCommand) {
	return "update player_state set current_list='".$currentList."', current_song=".$currentSong.
			", command='".$currentCommand."' where player_id=".$playerId.";";
}

function query_insert_sync_stats($playerId, $currentList, $currentSong, $currentCommand, $status){
return "insert into sync_stats set current_list='".$currentList."', current_song=".$currentSong.
			", command='".$currentCommand."', status='".$status."', sync_timestamp=CURRENT_TIMESTAMP, player_id=".$playerId.";";


}

function query_update_additional_data($playerId,$additionalData){
		return "update player_state set additional_data='".$additionalData."' where player_id=".$playerId.";";

}


function db_create_structure () {
	global $dbhost;
	global $dbuser;
	global $dbpass;
	global $dbname;
	global $playerId;

	echo "Go";
	mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
	mysql_select_db($dbname) or die(mysql_error());
	$queryResult = mysql_query("create table player_state (player_id INT, current_list VARCHAR(255), ".
		"current_song INT, command VARCHAR(50), status VARCHAR(50), timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP);");
	if ($queryResult == false) {
		echo $query;
		echo mysql_errno($link).": ".mysql_error($link)."<br/>";
	}
}

function db_get_player_id($device_key){

	$query="select player_id from device_keys where device_key='".strtoupper($device_key)."'";
	$r=mysql_query($query);

	if(mysql_num_rows($r)>0){

		$row=mysql_fetch_assoc($r);
		return $row['player_id'];

	} else { return null;}
}


function db_get_device_key($player_id){

	$query="select device_key from device_keys where player_id=".$player_id."";
	$r=mysql_query($query);

	if(mysql_num_rows($r)>0){
		$row=mysql_fetch_assoc($r);
		return $row['device_key'];
	} else { return null;}
}

function get_playlists_from_csv()
{
		return read_csv ($_SERVER['DOCUMENT_ROOT']."/data/playlists.csv");
}

function get_song_title_from_csv($playlist,$song_number,$playlists=false){

	if(!$playlists) {$playlists = read_csv ($_SERVER['DOCUMENT_ROOT']."/data/playlists.csv");}

	$data = read_csv ($_SERVER['DOCUMENT_ROOT']."/data/".$playlists[$playlist],true);

	return $data[$song_number]['title'];
	}


function cmdfm_getCurrentPlaylistUrls($playerId){
	$query="select * from app_cmdfm where player_id=".$playerId;
	$r=mysql_query($query);

	$playlistArrayResult=array();
	$playlistArray=array();

	if($f=mysql_fetch_array($r)){

		$playlistArray=json_decode($f['playlist_data'],true);

		for($i=0;$i<count($playlistArray);$i++){

			$playlistArrayResult[$i]=$playlistArray[$i]['url'];
		}


		return  $playlistArrayResult;


	}

	return null;
}

function cmdfm_get_CurrentSongTitle($playerId,$song_number, $playlistcontents=false){

	//if(!$playlistcontents) {$playlistcontents=cmdfm_getCurrentPlaylistUrls($playerId);}

	$query="select * from app_cmdfm where player_id=".$playerId;
	$r=mysql_query($query);

	$playlistArrayResult=array();
	$playlistArray=array();

	if($f=mysql_fetch_array($r)){

		$playlistArray=json_decode($f['playlist_data'],true);

		return array("listTitle"=> $f['genre'], "songTitle"=>$playlistArray[$song_number]['title']);
	}


}


function cmdfm_make_playlistSongData($cmdfmPlaylistJson){
	global $cmdfm_clientIdUrlParam;

	$cmdfmPlaylist=array();
	$cmdfmPlaylistResult=array();
	$cmdfmPlaylist=json_decode($cmdfmPlaylistJson,true);

	$i=0;
	foreach($cmdfmPlaylist as $index => $cmdfmSong)	{

		if($cmdfmSong['is_streamable']=='true') {
		$cmdfmPlaylistResult[$i]['title']=$cmdfmSong['title'];
		$cmdfmPlaylistResult[$i]['url']=$cmdfmSong['stream_url'].'?'.$cmdfm_clientIdUrlParam;

		$i++;
		}
	}

	return $cmdfmPlaylistResult;

}

function cmdfm_setCurrentPlaylist($playerId,$currentGenre,$cmdfmData){

	$cmdfmData=addslashes($cmdfmData);

	if(mysql_num_rows(mysql_query("select * from app_cmdfm where player_id=".$playerId.""))==0){
		//print_r(mysql_error());
		$q="insert into app_cmdfm set player_id=".$playerId;
		$r=mysql_query($q);

	}


	$query="select name from app_cmdfm_genres where slug='".$currentGenre."' limit 1";
	$f=mysql_fetch_array(mysql_query($query));





	$query="update app_cmdfm set genre='".$f['name']."'".",playlist_data=\"".$cmdfmData."\" where player_id=".$playerId;
	//echo $query;
	mysql_query($query);
	//echo mysql_error();

}



?>
