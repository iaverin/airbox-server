<?php

class Status {
	const INIT = "init";
	const PLAYING = "playing";
	const STOPPED = "stopped";
}

class Command {
	const PLAY = "play";
	const OK = "ok";
	const STOP = "stop";
}


$cmdfm_clientId='41798f76394a0030971f36dfb43cf46f';
$cmdfm_clientIdUrlParam="client_id=".$cmdfm_clientId;

$request_user_agent="Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36";

$cmdfm_request_searchUrl="https://cmd.to/api/v1/apps/fm/genres/";

$_8tracks_devId="e28ccc7f57ef4e69423e5e6fb260c37ff8235038";

$_8tracks_play_url="http://" . $_SERVER ["SERVER_NAME"]."/8tr_redir.php?play=1";
$_8tracks_next_url="http://" . $_SERVER ["SERVER_NAME"]."/8tr_redir.php";

//$cmdfm_genres=Array("New Wave","Heavy Metal","Disco","Drum & Bass","");

$RADIO_ICON_BASEURL="http://" . $_SERVER ["SERVER_NAME"]."/radio_pic.php?station_id=";

$dirble_api_key="c36b88093544c53914bc618b286f224ebd7089ce";

//$appsList=array("list","cmdfm","8tracks","dirble","radio");

$appsOptions=array(
	
	"default" => array("cache" => 0, "cleanCacheAfterPLay" => true,"allowNext"=>true  ),
 	
 	"cmdfm"   => array("cache" => 3, "cleanCacheAfterPLay" => true,"allowNext"=>true, "cacheTrashholdMb"=>50  ),
	"list"    => array("cache" => 1, "cleanCacheAfterPLay" => true,"allowNext"=>true, "cacheTrashholdMb"=>50  ),
	"8tracks" => array("cache" => 3, "cleanCacheAfterPLay" => true,"allowNext"=>true, "cacheTrashholdMb"=>50 ),
	"radio"   => array("cache" => 0, "cleanCacheAfterPLay" => true,"allowNext"=>false  )
	
	);

$API_EXTERNAL_APP_URL_LIST = array(

	"test" => array("url" => "http://" . $_SERVER ["SERVER_NAME"]."/test_api/cmdfm_test.php" , "appTitle" => "Test App")


	);





?>