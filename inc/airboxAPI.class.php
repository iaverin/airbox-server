<?php

require_once "util.php";
require_once "enums.php";


class airboxAPI {

	var $playerId;

	var $contentURI; //Airbox URI of content item - the URI class in util.php

	var $application; //applicatoin id in enums.php. key of $API_EXTERNAL_APP_URL_LIST

	var $appTitle;

	var $totalTracks;
	var $playlistTitle;

	var $addToPlayURL; // external URI of add to play service
	var $getPlaylistURL;  // external URI of gettin the playlist

	var $tracksInfo; 


	
	//var $application_list;

	function airboxApi($playerId,$contentUri=""){
	
			
		$this->playerId=$playerId;
		
		if($contentUri!="") {$this->setContentURI($contentUri);}
		
	}

	
	//update the airbox_playlist table. the main state table updates in ui_command.php

	function addToPlay($contentURI){

		//$genre=urlencode(substr($currentList, strlen("cmdfm://")));
		//echo $this->addToPlayURL."?airbox_uri=".urlencode($this->contentURI);

		$this->setContentURI($contentURI);

		$device_key=db_get_device_key($this->playerId);
		
		$s=httpRequest($this->addToPlayURL."?airbox_uri=".urlencode($contentURI)."&device_key=".$device_key);

		
		if($s!=""){
			
			$requestResult=array();

			$requestResult= json_decode($s,true);
		

			if($requestResult['result']=="ok"){


				$trackData=addslashes(json_encode($requestResult['play_list_data']));

				if(mysql_num_rows(mysql_query("select * from airbox_playlist where player_id=".$this->playerId.""))==0){
					$q="insert into airbox_playlist set player_id=".$this->playerId;
					$r=mysql_query($q);
					
				}


				$query="update airbox_playlist set airbox_content_uri='".$contentURI."', title='".$requestResult['title']."'".", total_tracks='".$requestResult["total_tracks"]."', playlist_data=\"".$trackData."\" where player_id=".$this->playerId;
				mysql_query($query);

				//$this->contentURI=$contentURI;

				$this->tracksInfo=$requestResult['play_list_data'];
				
				$this->totalTracks=count($this->tracksInfo);			

				$this->playlistTitle=$requestResult['title'];

				//$this->setContentURI($contentURI);
	


				return true;

			} else {
				
			//$this->getCurrentStatus();

				return false;

			}




		} else {

			return false; }
		
	}

	//
	//returns the array with URLS of playlist entires
	//todo: refacror to one function getCurrentStatus
	// 

	function getCurrentPlaylistUrls(){

		$query="select * from airbox_playlist where player_id=".$this->playerId;
		$r=mysql_query($query);

		$playlistArrayResult=array();
		$playlistArray=array();

		if($f=mysql_fetch_array($r)){

			$playlistArray=json_decode($f['playlist_data'],true);

			for($i=0;$i<count($playlistArray);$i++){
				
				$playlistArrayResult[$i]=$playlistArray[$i]['url'];
			}

			return  $playlistArrayResult;
		}

		return null;
	}

	// get current db record associated with playerId

	function getCurrentStatus(){

		$cuurentStatus=array();

		$query="select * from airbox_playlist where player_id=".$this->playerId;
		$r=mysql_query($query);

		if( $f=mysql_fetch_array($r)){ 

			$this->tracksInfo=json_decode($f['playlist_data'],true);

			$this->totalTracks=count($this->tracksInfo);			

			$this->playlistTitle=$f['title'];

			$this->setContentURI($f['airbox_content_uri']);

			return true;


		} else{
			
			return false;
		}

	}


	function setContentURI($contentURI){

		global $API_EXTERNAL_APP_URL_LIST;

		$this->contentURI = new uri($contentURI);	
		$this->application= $this->contentURI-> host;

		$this->addToPlayURL=$API_EXTERNAL_APP_URL_LIST[$this->application]['url'];
		$this->appTitle=$API_EXTERNAL_APP_URL_LIST[$this->application]['appTitle'];

		

	}


}


?>