<?php
require_once "enums.php";
require_once "get_country.php";


class dirbleClient{


var $apiKey;
var $httpBaseApiUrl="http://api.dirble.com/v1/"; 

var $httpBaseApiUrlV2="http://api.dirble.com/v2/";


var $DRIBLE_CONTINENTS=array("asia","europe","north-america","oceania","south-america","africa");

const MAX_KBPS="128";

var $currentStation=array();

function dirbleClient(){
	global $dirble_api_key;
	$this->apiKey=$dirble_api_key;
}


function getAllCategoriesJson(){


	$url=$this->httpBaseApiUrl."categories/apikey/".$this->apiKey;
	
	$s=file_get_contents ($url);

	return $s; 
}


function getAllCategories($doJSONDecode=false)
{

		
	$ctx = stream_context_create(array( 
    'http' => array( 
        'timeout' => 20
        ) 
    ) 
  ); 



	$url=$this->httpBaseApiUrlV2."categories/?token=".$this->apiKey;

	$s=file_get_contents ($url,0,$ctx);

	if(doJSONDecode==false)
		{return $s;}
	
	else{
	return json_decode($s,true); }


}

function getAllCountries($doJSONDecode=false){


	$ctx = stream_context_create(array( 
    'http' => array( 
        'timeout' => 20
        ) 
    ) 
  ); 

	$url=$this->httpBaseApiUrlV2."countries/?token=".$this->apiKey;

	//print_r($url);
	$countries=array();
	$countries_out=array();
	$s="";

	$s=file_get_contents($url,0,$ctx);

	

	$countries=json_decode($s,true);

	foreach ($countries as $key=>$value) {
		$countries_out[]=array("title" => country_code_to_country($value["country_code"]), "country" => $value["country_code"]);}


	if(doJSONDecode==false)
		{return json_encode($countries_out);}
	
	else{
	return $countries_out; }


}




function getStationsByContinent($continentUrl){
	
	$url=$this->httpBaseApiUrl."continent/apikey/".$this->apiKey."/continent/".$continentUrl;
	$s=file_get_contents ($url);
	return json_decode($s,true);

}

function getStationsByCountry($countryCode,$doJSONDecode=true){
	
	$url=$this->httpBaseApiUrl."country/apikey/".$this->apiKey."/country/".$countryCode;
	$s=file_get_contents ($url);


	if($doJSONDecode) { return json_decode($s,true);}
	else {return $s;}
}









function getStationsByCategory($categoryId,$doJSONDecode=true){
	
	$url=$this->httpBaseApiUrl."stations/apikey/".$this->apiKey."/id/".$categoryId;
	$s=file_get_contents ($url);

	if($doJSONDecode) { return json_decode($s,true);}
	else {return $s;}

}



function getStationsByRequest($request,$page=1,$per_page=30) { 


	//$page=1;
	//$per_page=30;

	$stations=array(1);
	$stations_active=array();
	$stations_all=array();

	// Max 20 stations and 2 requests 

	while(count($stations)>0 && (/*$page < 5 && */count($stations_active)<20 ))
	{

		//print_r(count($stations_active)."<br>");	
		$url=$request."?token=".$this->apiKey."&page=".$page."&per_page=".$per_page;
		
		


			$ctx = stream_context_create(array( 
    'http' => array( 
        'timeout' => 20
        ) 
    ) 
  ); 


		$s=file_get_contents ($url,0,$ctx);
		

		$stations=json_decode($s,true);

			foreach ($stations as  $station) {
				$stations_all[]=$station;

				foreach ($station['streams'] as $key => $value) {
						if($value['status']=="1"){
							$stations_active[]=$station;
							break;
						} 
					}	
				}
			
		$page+=1;
	
	}


	return $stations_active;

 }



function getActiveStationsByCategory($categoryId,$doJSONDecode=true){
	

	$request=$this->httpBaseApiUrlV2."category/".$categoryId."/stations";
	$stations_active=$this->getStationsByRequest($request);

	
	if($doJSONDecode) { return $stations_active;}
	else {return json_encode($stations_active);}
	
}



function getActiveStationsByCountry($countryCode,$doJSONDecode=true){
	
	$request=$this->httpBaseApiUrlV2."countries/".$countryCode."/stations";
	$stations_active=$this->getStationsByRequest($request);

	
	if($doJSONDecode) { return $stations_active;}
	else {return json_encode($stations_active);}

}




function getStationsByKeyWord($keyWord){

	$url=$this->httpBaseApiUrl."search/apikey/".$this->apiKey."/search/".urlencode($keyWord)."/count/5";
//	echo $url;
	$s=file_get_contents ($url);
	return json_decode($s,true);

}


function getStation($stationId){

	
	$ctx = stream_context_create(array( 
    'http' => array( 
        'timeout' => 20
        ) 
    ) 
  ); 





	$url=$this->httpBaseApiUrlV2."station/".$stationId."?token=".$this->apiKey;


	$s=file_get_contents ($url,0,$ctx);

	/*
	$s='{"id":10716,"name":"Discover Trance Radio","country":"AF",
	"image":{"url":null,"thumb":{"url":null}},"slug":"discover-trance-radio",
	"website":"http:\/\/www.discovertrance.com",
	"categories":[{"id":1,"title":"Trance","description":"stations that plays commercial and other things in trance-music genre.","slug":"trance","ancestry":"14"},{"id":3,"title":"Dance","description":"dance music, the new from 80s and 90s, like bubblegum and more.","slug":"dance","ancestry":"14"},{"id":14,"title":"Electronic","description":"all computeriz made.","slug":"electronic","ancestry":null}],
	"streams":[{
		"stream":"http:\/\/uk02.promo-cloud.com:8000","bitrate":0,"content_type":"?","status":0},
		{"stream":"http:\/\/norway.discovertrance.com:8000","bitrate":320,"content_type":"audio\/mpeg\r\n","status":1},
		{"stream":"http:\/\/chicago.discovertrance.com:9214","bitrate":192,"content_type":"audio\/mpeg\r\n","status":1},
		{"stream":"http:\/\/london01.discovertrance.com:80","bitrate":128,"content_type":"audio\/mpeg\r\n","status":1},
		{"stream":"http:\/\/hathor.discovertrance.com:8006","bitrate":155,"content_type":"audio\/mpeg\r\n","status":1}],
		"created_at":"2014-04-04T18:29:24+02:00","updated_at":"2015-04-11T14:17:13+02:00"}';
	*/

	if($s){



	
		$this->currentStation=json_decode($s,true);

		//print_r($this->currentStation);

		$prev_kbps=0;



		foreach ($this->currentStation["streams"] as $stream) {
				
			if($stream['status']=="1") {
				

				if($stream['bitrate']>$prev_kbps && $stream['bitrate']<=$this::MAX_KBPS){
				$prev_kbps=$stream['bitrate'];

				$this->currentStation["streamurl"]=$stream['stream'];}
				} 

				if($this->currentStation["streamurl"]=="") {
					$this->currentStation["streamurl"]=$stream['stream'];
				}
		
		}

		//print_r($this->currentStation);

		if($this->currentStation['streamurl']!=""){
			return json_decode($s,true);
		
		}
		else {
			return false;}	

	}
	
	else {
		return false;
	}


}


function getAllGenresFromDbJSON(){

	$genres=array();

	$query="select * from app_dirble_category_active_staions order by name asc ";
	$r=mysql_query($query);
	while($f=mysql_fetch_array($r)){
		$genres[]=$f;
	}

	return json_encode($genres);
}


function getCountriesFromDbJSON(){

	$genres=array();

	$query="select * from app_dirble_country_active order by name asc ";
	$r=mysql_query($query);
	while($f=mysql_fetch_array($r)){
		$genres[]=$f;
	}

	return json_encode($genres);
} 

function setToPlay($playerId,$stationId){

	if(!$this->getStation($stationId)) {
		//print_r($this);	
		return false;
	}
	

	if(mysql_num_rows(mysql_query("select * from app_dirble where player_id=".$playerId.""))==0){
		$q="insert into app_dirble set player_id=".$playerId;
		$r=mysql_query($q);
		
	}
	
	
	$q="update app_dirble set station_id=".$stationId.", title='".$this->currentStation['name']."', stream_url='".$this->currentStation['streamurl']."' where player_id=".$playerId;
	
	
	mysql_query($q);
	return true;

}


function getStationFromDb($playerId){

	$query="select * from app_dirble where player_id=".$playerId;
	$r=mysql_query($query);

	$f=mysql_fetch_array($r);

	$this->currentStation=$f;

}

}


?>
