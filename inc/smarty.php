<?php
require 'inc/smarty/libs/Smarty.class.php';
//ob_flush();
function render ($data, $template) {	
	#TODO extract to server properties
	date_default_timezone_set ("Europe/Moscow");

	$smarty = new Smarty;
	$smarty->template_dir = 'templates/';
	$smarty->compile_dir  = 'templates_c/';
	#$smarty->config_dir   = '/web/www.example.com/guestbook/configs/';
	#$smarty->cache_dir    = '/web/www.example.com/guestbook/cache/';
	foreach ($data as $key => $value) {
		$smarty->assign ($key, $value);
	}
	$smarty->display($template);
}

?>