<?php
session_start ();
require 'inc/util.php';
require 'inc/db.php';
require 'inc/smarty.php';
require 'inc/enums.php';

if ($_SESSION ['player_id'] == "") {
	header ( "Location: http://" . $_SERVER ["SERVER_NAME"] . ":" . $_SERVER ["SERVER_PORT"] . "/login.php" );
} else {
	
	$playerId = $_SESSION ['player_id'];
}

// -----------------------------------------------------------------------------------------------
// Set net new playlist/song after command PLAY or INIT status and send full
// playlist to player
// Set commands to DataBase
// ------------------------------------------------------------------------------------------------

$dbCurrent = db_get_current ();
$currentList = $dbCurrent ["current_list"];
$currentSong = $dbCurrent ["current_song"];

if (isset ( $_GET ["command"] )  && (isset ( $_GET ["current_list"] ) || isset ( $_GET ["current_song"] )   )) {
	
	// set the  list selected
	if(isset ( $_GET ["current_list"] )){ $currentList = htmlspecialchars_decode($_GET ["current_list"]); }
	if(isset ( $_GET ["current_song"] )){ $currentSong =  $_GET ["current_song"];}
	
	
	$currentCommand = $_GET ["command"];
	
	$dbNew = array ();
	$dbNew ["current_list"] = $currentList;
	
	$dbNew ["current_song"] = $currentSong;
	$dbNew ["command"] = $currentCommand;
	
	if (isset ( $_GET ["status"] )) {
		$dbNew ["status"] = $_GET ["status"];
	}
	
	//reload the list from cmd.fm if only sonf changes than not reload the list
	if(isset($_GET ["current_list"])){
	$genre=urlencode(substr($currentList, strlen("cmdfm://")));
	$s=file_get_contents ($cmdfm_request_searchUrl.$genre);
	
	
	$playlistData=json_encode(cmdfm_make_playlistSongData($s));
		
	cmdfm_setCurrentPlaylist($playerId, substr($currentList, strlen("cmdfm://")), $playlistData);
	
	}
	
	db_update_current ( $dbNew );
	// todo ���������� ������� �� ����� ����������
	
	header ("Access-Control-Allow-Origin: *");	
	header ( "Location: http://" . $_SERVER ["SERVER_NAME"] . ":" . $_SERVER ["SERVER_PORT"] . "/ui_cmdfm.php");
	exit ();
}

// -------------------------------
// Output UI, current list/song
// ---------------------------------


$currentList = $dbCurrent ["current_list"];
$currentSong = $dbCurrent ["current_song"];
$currentCommand = $dbCurrent ["command"];
$status = $dbCurrent ["status"];

// Get List to show
if (isset ( $_GET ["current_list"] ) && strpos($_GET ["current_list"],"cmdfm://")===false)  {
	$currentListShow = htmlspecialchars_decode ( $_GET ["current_list"] );
}

// echo get_song_title($currentList,$currentSong);

// ob_flush();
// var_dump($playlists);

// ----------------------------------------
// Fill the array to pass for render
$data = array ();

$data ['player_id'] = $playerId;
$data ['device_key'] = $_SESSION ['device_key'];
$data ["currentList"] = $currentList;
$data ["currentSong"] = $currentSong;
$data ["status"] = $status;
$data ["command"] = $currentCommand;
$data ["timestamp"] = $dbCurrent ["time_diff"];


if(isset($_GET['show_genres'])) {
$playlists = $cmdfm_genres;
$data ['playlists'] = $playlists; // odo escape

}





if (strpos($currentList,"cmdfm://")!==false)
	{
		
		if ($currentSong < count(cmdfm_getCurrentPlaylistUrls($playerId)) ) { $data['next_track']=$currentSong+1;}
		
		$data ['currentSongTitle'] = cmdfm_get_CurrentSongTitle($playerId, $currentSong );
	//	$data ['listContents']
	}
	
	else{
		

			//$data ['currentSongTitle'] = get_song_title_from_csv ( $currentList, $currentSong );
	
		
		   
		
	}
	

	if(isset($currentListShow))	{$data ['listContents'] = get_playlist_contents_to_show($currentListShow);}
	$data ["currentListShow"] = $currentListShow;	
	



 // n seconds
                                              // ----------------------------------------
                                              // Show
render ( $data, "ui_cmdfm.tpl" );




function get_playlist_contents_to_show($playlist) {
	global $playerId, $playlists;

	if (strpos ( $currentList, "cmdfm://" ) !== false) {
		return cmdfm_getCurrentPlaylistUrls ( $playerId );
	}
	
	
	return read_csv ( "data/" . $playlists [$playlist], true );
	
	
}




?>



