<?php
require 'inc/db.php';
require_once 'inc/util.php';
require 'inc/enums.php';
require "inc/8tracks.php";
require "inc/radio.php";
require "inc/dirble.class.php";
require 'inc/airboxAPI.class.php';
require "inc/history.class.php";


if (!isset($_GET["status"]) && (!isset($_GET["current_list"]) && !isset($_GET["current_song"]))) {
	$newState ["error"] = "Invalid request: status or (current_list, current_song) should be specified";
	echo json_encode($newState);
	exit();
}


//[statistic] => {"dkey":"BAMS";"currentList":"cmdfm://80s";"currentTrack":20;"currentTrackTime":290;"currentTrackTotalTime":368;"cacheStatus":[{"index":20;"state":"PLAY";"cache":100};{"index":21;"state":"Q";"cache":100};{"index":22;"state":"Q";"cache":100}]}


if(isset($_POST['statistic'])) $a=str_replace(";", ",", $_POST['statistic']);


//if(isset($_POST['statistic'])) $a=$_POST['statistic'];
//$s=gettype($a);

//foreach ($a as $key => $value )
//{ print_r($key."=>".$value, true)
//file_put_contents("all_requests.1",print_r($_REQUEST,true)."\n\r",  FILE_APPEND );
//}

// just to update the stats from client
// nothing to do more

if(isset($_POST['statistic'])) {

	$clientData=json_decode($_POST['statistic']);
	$device_key=$clientData->dkey;
	$playerId=db_get_player_id($device_key);
	mysql_query(query_update_additional_data($playerId,$a));
	exit(0);
}


$device_key=$_GET['dkey'];

$playerId=db_get_player_id($device_key);


$newState = array();
$dbCurrent = db_get_current();
$currentList = $dbCurrent["current_list"];
$currentSong = $dbCurrent["current_song"];
$currentCommand = $dbCurrent["command"];

//$dbCurrentAdditionalData=$dbCurrent["additional_data"];

$dbCurrent["additional_data"]=$a;





#var_dump ($dbCurrent);
if (isset ($currentCommand) && strcmp($currentCommand, Command::STOP) == 0) {
	$newState ["current_list"] = $currentList;
	$newState ["current_song"] = $currentSong;
	$newState ["command"] = Command::STOP;

	$dbCurrent ["status"] = Status::STOPPED;
}
elseif ((isset ($_GET["status"]) && strcmp($_GET["status"], Status::INIT) == 0) ||
	(isset ($currentCommand) && strcmp($currentCommand, Command::PLAY) == 0)) {
	# either player is starting and sends us status "init" or user commanded to play
	#TODo check if we are commanding to play the same song?

	//$appName = substr($currentList, 0, strpos($currentList,"://"));
	//if($appName=="") $appName="default";


	//$appsOptions=getAppsOptions($currentList));

	// addiong playlist options

///	if(isset($_GET["current_song"])) { $currentSong=$_GET['current_song'] ; }
///	if(isset($_GET["current_list"])) { $currentList=$_GET['current_list'] ; }


	$newState['playListOptions']=getAppsOptions($currentList);



	if(strpos($currentList,"cmdfm://")!==false){

		$newState ["current_list"]=$currentList;

		$listContents=cmdfm_getCurrentPlaylistUrls($playerId);
		$newState ["current_song"] = $currentSong;

    $currentListTitle=substr($currentList, strlen("cmdfm://"));

  }

	elseif(strpos($currentList,"8tracks://")!==false){

		$newState ["current_list"]=$currentList;
		$listContents=_8tracks_getCurrentPlayListUrls($playerId);


		$listContents=_8tracks_getCurrentPlayListUrls($playerId);

		$newState ["current_song"] = $currentSong;

    $currentListTitle =_8tracks_get_CurrentMixInfo($playerId)['name'];

	}
	elseif(strpos($currentList,"radio://")!==false){

		$newState ["current_list"]=$currentList;

		$stationId=substr($currentList, strlen("radio://"));
		$listContents=radio_getCurrentPlayListUrls($stationId);

		$newState ["current_song"] = $currentSong;

    $currentListTitle =radio_getStationTitle($stationId );

	}

	elseif(strpos($currentList,"dirble://")!==false){

		$newState ["current_list"]=$currentList;

		$stationId=substr($currentList, strlen("dirble://"));


		$dirble=new dirbleClient();
		$dirble->getStationFromDb($playerId);

		$listContents=array($dirble->currentStation['stream_url']);

		$newState ["current_song"] = $currentSong;

    $currentListTitle =$dirble->currentStation['title'];


	}

	elseif(strpos($currentList,"airbox://")!==false){

		$newState ["current_list"]=$currentList;

		$api=new airboxAPI($playerId);


		$listContents=$api->getCurrentPlaylistUrls();
		$newState ["current_song"] = $currentSong;

    $currentListTitle = $api->playlistTitle;

	}
	else{

	$newState ["current_list"] = $currentList;
	$newState ["current_song"] = $currentSong;

	$playlists = read_csv ("data/playlists.csv");
	$filename = $playlists[$currentList];
	$listContents = read_csv ("data/".$filename);

  $currentListTitle=$currentList;

	}

	$newState ["command"] = Command::PLAY;

	$dbCurrent ["status"] = Status::PLAYING;

	$newState ["list_contents"] = $listContents;



//  $history=new abHistory($playerId);

//	$remoteCurrentSong = $_GET["current_song"];
//	$remoteCurrentList = $_GET["current_list"];
//	$remoteStatus = $_GET["status"];



}
else {
	# if playing another track in the list, change current song and confirm it back
	$remoteCurrentSong = $_GET["current_song"];
	$remoteCurrentList = $_GET["current_list"];
	$remoteStatus = $_GET["status"];

	if (strcmp($remoteCurrentSong, $currentSong) != 0 || strcmp($remoteCurrentList, $currentList) != 0) {
		#For now we will trust player that it knows what it plays if no command was given
		$currentSong = $remoteCurrentSong;
		$currentList = $remoteCurrentList;
		$dbCurrent ["current_list"] = $remoteCurrentList;
		$dbCurrent ["current_song"] = $remoteCurrentSong;
	}
	$newState ["current_list"] = $currentList;
	$newState ["current_song"] = $currentSong;
	$newState ["command"] = Command::OK;

	//$dbCurrent["additional_data"]

	$dbCurrent ["status"] = $remoteStatus;
}

#reset command, we sent it
$dbCurrent["command"] = Command::OK;
#var_dump ($dbCurrent);
db_update_current($dbCurrent);

//$newState['position']=30; - set the position for start the current position is in position from device

echo json_encode($newState);
?>
