<?php
session_start ();
require 'inc/util.php';
require 'inc/db.php';
require 'inc/smarty.php';
require 'inc/enums.php';
require 'inc/8tracks.php';
require 'inc/dirble.class.php';
require 'inc/airboxAPI.class.php';
require "inc/history.class.php";



//error_reporting( E_NONE );

header ("Access-Control-Allow-Origin: *");



$result=array();

if ($_GET['device_key'] != "") {

	$device_key=mysql_real_escape_string($_GET['device_key']);

	$playerId=db_get_player_id($device_key);


} else {

		$result['error']="device_key is not set";
		echo json_encode($result);
		exit ();
	}

// -----------------------------------------------------------------------------------------------
// Set net new playlist/song after command PLAY or INIT status and send full
// playlist to player
// Set commands to DataBase
// ------------------------------------------------------------------------------------------------

$dbCurrent = db_get_current ();
$currentList = $dbCurrent ["current_list"];
$currentSong = $dbCurrent ["current_song"];

if (isset ( $_GET ["command"] )  && (isset ( $_GET ["current_list"] ) || isset ( $_GET ["current_song"] )   )) {

	// set the  list selected
	if(isset ( $_GET ["current_list"] )){ $currentList = htmlspecialchars_decode($_GET ["current_list"]); }
	if(isset ( $_GET ["current_song"] )){ $currentSong =  $_GET ["current_song"];}


	$currentCommand = $_GET ["command"];

	$dbNew = array ();
	$dbNew ["current_list"] = $currentList;

	$dbNew ["current_song"] = $currentSong;
	$dbNew ["command"] = $currentCommand;

	if (isset ( $_GET ["status"] )) {
		$dbNew ["status"] = $_GET ["status"];
	}

	//reload the list from cmd.fm if only sonf changes than not reload the list
	if(isset($_GET ["current_list"])){

		/// Set cmd.fm for play
		if (strpos($currentList,"cmdfm://")!==false)
		{
			$genre=urlencode(substr($currentList, strlen("cmdfm://")));

			
			$s=httpRequest($cmdfm_request_searchUrl.$genre);



			$playlistData=json_encode(cmdfm_make_playlistSongData($s));
			cmdfm_setCurrentPlaylist($playerId, substr($currentList, strlen("cmdfm://")), $playlistData);
			

		    $currentListTitle=substr($currentList, strlen("cmdfm://"));


			$result['result']="ok";


		}

	elseif (strpos($currentList,"8tracks://")!==false)
		{

	$mixId=urlencode(substr($currentList, strlen("8tracks://")));

	//$s=file_get_contents ($cmdfm_request_searchUrl.$genre);
	//print_r($mixId);

	$mix=_8tracks_getMixInfo($mixId);

	_8tracks_setMix($mix);

	$currentListTitle =_8tracks_get_CurrentMixInfo($playerId)['name'];

	$result['result']="ok";

	}

	elseif (strpos($currentList,"dirble://")!==false)
		{

	$stationId=substr($currentList, strlen("dirble://"));


	$dirble=new dirbleClient();

	if(!$dirble->setToPlay($playerId,$stationId)) { $result['result']="error";    }





	else{
			$currentListTitle =$dirble->currentStation['title'];

			$result['result']="ok";
	}


	}

	elseif(strpos($currentList,"airbox://")!==false){


		$api=new airboxAPI($playerId);


		if(!$api->addToPlay($currentList)) { $result['result']="error";    }



	}

		else {
			$result['result']="ok"; 
			$currentListTitle = $api->playlistTitle;

		}

	}

	if($result['result']!="error"){

	$result['result']="ok";

	db_update_current ( $dbNew );
	if($currentList!=$dbCurrent ["current_list"]){
		$history=new abHistory($playerId);
		$history->addHistoryRecord($currentList,  $currentListTitle);
		}


	};

	echo json_encode($result);
	exit ();

}



?>
