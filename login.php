<?
session_start();
require 'inc/util.php';
require 'inc/db.php';
require 'inc/smarty.php';
require 'inc/enums.php';

$data=array();

$action=$_GET['action'];

if(isset($_SESSION['player_id'])) {
	
	$player_id=$_SESSION['player_id'];
	$data['player_id']=$player_id;
	$data['device_key']=db_get_device_key($player_id);
} 

if($action=="auth_device_key"){

	$device_key=$_GET['device_key'];

	$player_id=db_get_player_id($device_key); 
	
	if ($player_id!== null ) {

		$_SESSION['player_id']=$player_id;
		$_SESSION['device_key']=db_get_device_key($player_id);

		$data['player_id']=$player_id;
		$data['device_key']=db_get_device_key($player_id);
		header("Location: http://".$_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"]."/ui.php");
		exit(0);
	}

}

if($action=="reset_device_key"){
	unset($_SESSION['player_id']);
	$data=array();

}


render ($data, "login.tpl");

?>